const NodeList = require("../server/NodeList");
const path = require("path");
const stringSanitizer = require("string-sanitizer");
const fs = require("fs");

const nodeList = new NodeList();

const data = nodeList.getList();

const pathAuthor = path.join(__dirname, "/out/insert-authors.txt");
const pathBProduction = path.join(__dirname, "/out/insert-bProduction.txt");
const pathEdgesAuthorBProduction = path.join(__dirname, "/out/insert-edgesAuthorBProduction.txt");
const pathAreas = path.join(__dirname, "/out/insert-areas.txt");
const pathEdgesAuthorArea = path.join(__dirname, "/out/insert-edgesAuthorArea.txt");

let authors = {};
let bibliographicProduction = {};
let edgesAuthorBProduction = [];
let oAreas = [];
let edgesAuthorArea = [];

Array.prototype.unique = function() {
  var a = this.concat();
  for(var i=0; i<a.length; ++i) {
      for(var j=i+1; j<a.length; ++j) {
          if(a[i] === a[j])
              a.splice(j--, 1);
      }
  }

  return a;
};

data.forEach(function(project) {
    bibliographicProduction[project.title] = {
        title: stringSanitizer.sanitize.keepSpace(project.title),
        type: project.type,
        doi: project.doi,
        year: project.year,
    };

    project.authors.forEach(function(author) {
        authors[author.authorName] = {
            name: stringSanitizer.sanitize.keepSpace(author.authorName),
            resume: author.details ? stringSanitizer.sanitize.keepSpace(author.details.resume) : '',
            idCNPQ: author.idCNPQ
        }

        edgesAuthorBProduction.push("create edge Desenvolve from (select from Autor where name = '" + stringSanitizer.sanitize.keepSpace(author.authorName) +"') to (select from ProducaoBibliografica where title = '" + stringSanitizer.sanitize.keepSpace(project.title) +"'); \r\n")

        
        if(author?.details?.occupationArea){
          oAreas = oAreas.concat(author.details.occupationArea).unique();

          author.details.occupationArea.map((element) => {
            if(element){
              edgesAuthorArea.push("create edge Possui from (select from Autor where name = '" + stringSanitizer.sanitize.keepSpace(author.authorName) +"') to (select from AreaInteresse where name = '" + element +"') ;\r\n");
              edgesAuthorArea = edgesAuthorArea.unique();
            }
          })
        }
    });
});

try {
  deleteFile(pathAuthor);
  deleteFile(pathBProduction);
  deleteFile(pathEdgesAuthorBProduction);
  deleteFile(pathAreas);
  deleteFile(pathEdgesAuthorArea);

  Object.keys(authors).forEach(function(key, index) {
    fs.appendFile(
      pathAuthor,
      "UPDATE Autor SET name = '" + authors[key].name + "', resume = '" + authors[key].resume + "', idCNPQ = '" + authors[key].idCNPQ + "'  UPSERT WHERE name = '" + authors[key].name + "'; \r\n",      
      function (err) {
        if (err) throw err;
      }
    );
  });  
  
  Object.keys(bibliographicProduction).forEach(function(key, index) {    
    fs.appendFile(
      pathBProduction,
      "UPDATE ProducaoBibliografica SET title = '" + bibliographicProduction[key].title + "', type = '" + bibliographicProduction[key].type + "', year = '" + bibliographicProduction[key].year + "', doi = '" + bibliographicProduction[key].doi + "'  UPSERT WHERE type = '" + bibliographicProduction[key].title + "'; \r\n",
      function (err) {
        if (err) throw err;
      }
    );
  });

  edgesAuthorBProduction.forEach(function(edge){
    fs.appendFile(
      pathEdgesAuthorBProduction,
      edge,
      function (err) {
        if (err) throw err;
      }
    );
  });

  oAreas.forEach(function(area){
    fs.appendFile(
      pathAreas,
      "UPDATE AreaInteresse SET name = '" + area + "' UPSERT WHERE name = '" + area + "'; \r\n",
      function (err) {
        if (err) throw err;
      }
    );
  });

  edgesAuthorArea.forEach(function(edge){
    fs.appendFile(
      pathEdgesAuthorArea,
      edge,
      function (err) {
        if (err) throw err;
      }
    );
  });

} catch (e) {
  console.log("Erro ao gerar arquivos de insert. " + e);
}


function deleteFile(path){
  if (fs.existsSync(path)) {
    fs.unlink(path, function (err) {
      if (err) throw err;
      console.log("Arquivo apagado com sucesso.");
    });
  }
}