'use strict';
const gulp = require('gulp'),
      del = require("del"),
      sass = require('gulp-sass')(require('sass')),
      cssnano = require('gulp-cssnano'),
      sourcemaps = require('gulp-sourcemaps'),
      rename = require("gulp-rename"),
      nodemon = require("gulp-nodemon"),
      imagemin = require("gulp-imagemin"),
      autoprefixer = require('gulp-autoprefixer');

const { path } = require('express/lib/application');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');      

const paths = {
    styles: {
        src: "./server/views/sass/main.scss",
        lib: "",
        watch: "./server/views/sass/**/*.scss"
    },
    scripts: {
        src: "./server/views/js/main.js",
        watch: "./server/views/js/**/*.js"
    },
    output: "./dist",
    outputStatic: "./dist/assets",
    img: {
		src: "./server/views/img/**/*.{png,gif,jpg}",
		watch: "./server/views/img/**/*.{png,gif,jpg}",
	}
}

gulp.task('clean', function() {
    return del(['./dist']);
});

gulp.task('scripts', function() {
    const webpackConfig = require('./webpack/webpack.local.js');

	return gulp.src(paths.scripts.src)
		.pipe(webpackStream(webpackConfig), webpack)
		.on("error", function handleError() {
			this.emit("end");
		})
		.pipe(gulp.dest(paths.outputStatic));
});

gulp.task('styles', function(){
    return gulp.src(paths.styles.src)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
        cascade: false
        }))
        .pipe(cssnano())
        .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.outputStatic))
});

gulp.task('img', function() {
	return gulp
		.src(paths.img.src)
		.pipe(imagemin())
		.pipe(gulp.dest(paths.outputStatic))
});

gulp.task('watch', function () {
    gulp.watch(paths.styles.watch, { ignoreInitial: false }, gulp.series('styles'));
    gulp.watch(paths.scripts.watch, { ignoreInitial: false }, gulp.series('scripts'));
    gulp.watch(paths.img.watch, { ignoreInitial: false }, gulp.series('img'));
});

gulp.task('start', function (cb) {
    let started = false;
    const stream = nodemon({ script: './server/app.js',
            ext: 'html js',
            ignore: ['ignored.js']});
   
    stream
        .on('start', function(){
            if (!started) {
                cb();
                started = true; 
            } 
        })
        .on('restart', function () {
          console.log('restarted!')
        })
        .on('crash', function() {
          console.error('Application has crashed!\n')
           stream.emit('restart', 10)  // restart the server in 10 seconds
        })
})

exports.start = gulp.parallel(['start', 'watch']);
exports.build = gulp.series('clean', gulp.parallel('styles', 'scripts', 'img'));