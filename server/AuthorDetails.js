class AuthorDetails {
  constructor(element) {
    this.element = element;

    this.getGeneralData();
  }

  getGeneralData() {
    const generalData = this.element['DADOS-GERAIS'];
    const academicGraduation = generalData['FORMACAO-ACADEMICA-TITULACAO'];
    const occupationAreas = generalData['AREAS-DE-ATUACAO'];

    this.autor = {
      'name' : generalData['@_NOME-COMPLETO'],
      'resume' : generalData['RESUMO-CV']['@_TEXTO-RESUMO-CV-RH'],
      'idCNPQ': this.element['@_NUMERO-IDENTIFICADOR'],
      'academicGraduation': [],
      'occupationArea': []
    }
    this.getAcademicGraduation(academicGraduation);
    this.getOccupationArea(occupationAreas);
  }

  getAcademicGraduation(academicGraduation) {
    let graduation = academicGraduation['GRADUACAO']; 
    let specialization = academicGraduation['ESPECIALIZACAO'];
    let masters = academicGraduation['MESTRADO'];
    let doctorate = academicGraduation['DOUTORADO'];
    
    if(graduation !== undefined) this.getGraduationData(graduation);
    if(specialization !== undefined) this.getSpecializationData(specialization);
    if(masters !== undefined) this.getMastersData(masters);
    if(doctorate !== undefined) this.getDoctorateData(doctorate);    
  }

  getGraduationData(graduation) {
    if(graduation.length) {
      graduation.map((element) => {
        this.autor.academicGraduation.push({
          "type": "graduacao",
          "courseName" : element['@_NOME-CURSO'],
          "nameInstitution" : element['@_NOME-INSTITUICAO']
        })
      });
    } else {
      this.autor.academicGraduation.push({
        "type": "graduacao",
        "courseName" : graduation['@_NOME-CURSO'],
        "nameInstitution" : graduation['@_NOME-INSTITUICAO']
      })
    }
  }

  getSpecializationData(specialization) {
    if(specialization.length) {
      specialization.map((element) => {
        this.autor.academicGraduation.push({
          "type": "especializacao",
          "courseName" : element['@_NOME-CURSO'],
          "nameInstitution" : element['@_NOME-INSTITUICAO']
        })
      });
    } else {
      this.autor.academicGraduation.push({
        "type": "especializacao",
        "courseName" : specialization['@_NOME-CURSO'],
        "nameInstitution" : specialization['@_NOME-INSTITUICAO']
      })
    }
  }

  getMastersData(masters) {
    if(masters.length) {
      masters.map((element) => {
        this.autor.academicGraduation.push({
          "type": "mestrado",
          "courseName" : element['@_NOME-CURSO'],
          "nameInstitution" : element['@_NOME-INSTITUICAO']
        })
      });
    } else {
      this.autor.academicGraduation.push({
        "type": "mestrado",
        "courseName" : masters['@_NOME-CURSO'],
        "nameInstitution" : masters['@_NOME-INSTITUICAO']
      })
    }
  }

  getDoctorateData(doctorate) {
    if(doctorate.length) {
      doctorate.map((element) => {
        this.autor.academicGraduation.push({
          "type": "doutorado",
          "courseName" : element['@_NOME-CURSO'],
          "nameInstitution" : element['@_NOME-INSTITUICAO']
        })
      });
    } else {
      this.autor.academicGraduation.push({
        "type": "doutorado",
        "courseName" : doctorate['@_NOME-CURSO'],
        "nameInstitution" : doctorate['@_NOME-INSTITUICAO']
      })
    }
  }

  getOccupationArea(occupationAreas){
    try{
      if(occupationAreas){
        if(occupationAreas['AREA-DE-ATUACAO'].length){
          occupationAreas['AREA-DE-ATUACAO'].map((element) => {
            this.autor.occupationArea.push(element['@_NOME-DA-SUB-AREA-DO-CONHECIMENTO'])
          })
        }
      }
    }catch(e){
      console.warn('Erro ao obter área de atuação. ' + e);      
    }
  }

  getAuthorDetails() {
    return this.autor;
  }

  getAuthorsKeywordsNodes() {}
}

module.exports = AuthorDetails;