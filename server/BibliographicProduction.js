class BibliographicProduction {
  constructor(element, author, list) {
    this.element = element;
    this.author = author;
    this.productionList = list;

    this.getAllProduction();
  }

  getAllProduction() {
    let eventsWorks = this.element['TRABALHOS-EM-EVENTOS']; 
    let publishedArticles = this.element['ARTIGOS-PUBLICADOS'];
    let booksAndChapters = this.element['LIVROS-E-CAPITULOS'];
    
    if(eventsWorks !== undefined) this.getEventsWorks(eventsWorks);
    if(publishedArticles !== undefined) this.getPublishedArticles(publishedArticles);
    if(booksAndChapters !== undefined) this.getBooksAndChapters(booksAndChapters);
  }

  getEventsWorks(projects) {
    let eventsWorks = projects['TRABALHO-EM-EVENTOS'];
    let _this = this;
          
    if(eventsWorks.length){
      eventsWorks.map((element) => {
        const authorsList = element.AUTORES;
        const projectTitle = element['DADOS-BASICOS-DO-TRABALHO']['@_TITULO-DO-TRABALHO'];
        const projectYear = element['DADOS-BASICOS-DO-TRABALHO']['@_ANO-DO-TRABALHO'];
        const doi = element['DADOS-BASICOS-DO-TRABALHO']['@_DOI'];
                
        const project = {
          'title' : projectTitle,
          'type' : "trabalho-em-eventos",
          'year': projectYear,
          'doi': doi,
          'authors' : []
        }
                
        project.authors = _this.getAuthorsWorksNodes(authorsList);
        _this.productionList.push(project);
      });
    }
  }

  getPublishedArticles(articles) {    
    let publishedArticles = articles['ARTIGO-PUBLICADO'];
    let _this = this;
          
    if(publishedArticles.length){
      publishedArticles.map((element) => {
        const authorsList = element.AUTORES;
        const projectTitle = element['DADOS-BASICOS-DO-ARTIGO']['@_TITULO-DO-ARTIGO'];
        const projectYear = element['DADOS-BASICOS-DO-ARTIGO']['@_ANO-DO-ARTIGO'];
        const doi = element['DADOS-BASICOS-DO-ARTIGO']['@_DOI'];
                
        const project = {
          'title' : projectTitle,
          'type' : "artigo-publicado",
          'year': projectYear,
          'doi': doi,
          'authors' : []
        }
                
        project.authors = _this.getAuthorsWorksNodes(authorsList);
        _this.productionList.push(project);
      });
    }
  }

  getBooksAndChapters(booksAndChapters) {
    let books = booksAndChapters['LIVROS-PUBLICADOS-OU-ORGANIZADOS'];
    let chapters = booksAndChapters['CAPITULOS-DE-LIVROS-PUBLICADOS'];
    let _this = this;
          
    if(books !== undefined) {
      books = books['LIVRO-PUBLICADO-OU-ORGANIZADO'];

      if(books.length) {
        books.map((element) => {
          const authorsList = element.AUTORES;
          const title = element['DADOS-BASICOS-DO-LIVRO']['@_TITULO-DO-LIVRO'];
          const year = element['DADOS-BASICOS-DO-LIVRO']['@_ANO'];
          const doi = element['DADOS-BASICOS-DO-LIVRO']['@_DOI'];
                  
          const project = {
            'title' : title,
            'type' : "livro-publicado-organizado",
            'year': year,
            'doi': doi,
            'authors' : []
          }
                  
          project.authors = _this.getAuthorsWorksNodes(authorsList);
          _this.productionList.push(project);
        });
      } else if(books !== undefined) {
        const authorsList = books.AUTORES;
        const title = books['DADOS-BASICOS-DO-LIVRO']['@_TITULO-DO-LIVRO'];
        const year = books['DADOS-BASICOS-DO-LIVRO']['@_ANO'];
        const doi = books['DADOS-BASICOS-DO-LIVRO']['@_DOI'];
                
        const project = {
          'title' : title,
          'type' : "livro-publicado-organizado",
          'year': year,
          'doi': doi,
          'authors' : []
        }
                
        project.authors = _this.getAuthorsWorksNodes(authorsList);
        _this.productionList.push(project);
      }
    } 

    if(chapters !== undefined) {
      chapters = chapters['CAPITULO-DE-LIVRO-PUBLICADO'];

      if(chapters.length) {
        chapters.map((element) => {
          const authorsList = element.AUTORES;
          const title = element['DADOS-BASICOS-DO-CAPITULO']['@_TITULO-DO-CAPITULO-DO-LIVRO'];
          const year = element['DADOS-BASICOS-DO-CAPITULO']['@_ANO'];
          const doi = element['DADOS-BASICOS-DO-CAPITULO']['@_DOI'];
          const book = element['DETALHAMENTO-DO-CAPITULO']['@_TITULO-DO-LIVRO'];
          
          const project = {
            'title' : title,
            'type' : "capitulo-de-livro-publicado",
            'year': year,
            'doi': doi,
            'bookTitle': book,
            'authors' : []
          }
                  
          project.authors = _this.getAuthorsWorksNodes(authorsList);
          _this.productionList.push(project);
        });
      } else if(chapters !== undefined) {
        const authorsList = chapters.AUTORES;
        const title = chapters['DADOS-BASICOS-DO-CAPITULO']['@_TITULO-DO-CAPITULO-DO-LIVRO'];
        const year = chapters['DADOS-BASICOS-DO-CAPITULO']['@_ANO'];
        const doi = chapters['DADOS-BASICOS-DO-CAPITULO']['@_DOI'];
        const book = chapters['DETALHAMENTO-DO-CAPITULO']['@_TITULO-DO-LIVRO'];

        const project = {
          'title' : title,
          'type' : "capitulo-de-livro-publicado",
          'year': year,
          'doi': doi,
          'bookTitle': book,
          'authors' : []
        }
                
        project.authors = _this.getAuthorsWorksNodes(authorsList);
        _this.productionList.push(project);
      }
    }
  }

  getAuthorsWorksNodes(authorsList) {
    let authors = [];

    if(authorsList.length) {
      authorsList.forEach((element) => {
        let weight = 1;
        let idAuthor = this.author.idCNPQ;

        if(element['@_ORDEM-DE-AUTORIA'] == 1) weight = 2;
        
        let author = {
          'authorName' : element['@_NOME-COMPLETO-DO-AUTOR'],
          'weight': weight,
          'idCNPQ': element['@_NRO-ID-CNPQ']           
        }

        if(element['@_NRO-ID-CNPQ'] == idAuthor) {
          author['details'] =this.author
        }

        authors.push(author);
      });
    } else if (Object.keys(authorsList).length) {
       authors.push({
        'authorName' : authorsList['@_NOME-COMPLETO-DO-AUTOR'],
        'weight': 2,
        'details': this.author           
      })          
    }

    return authors
  }

  getAuthorsKeywordsNodes() {}
}

module.exports = BibliographicProduction;