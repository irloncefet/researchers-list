let fs = require('fs');
let parser = require('fast-xml-parser');
const path = require('path');
const BibliographicProduction = require('./BibliographicProduction');
const AuthorDetails = require('./AuthorDetails');

class NodeList{
  constructor(){
    this.researchersList = [];
    
    this.getAllFiles();
    this.getFileInfo();
  }

  getAllFiles(){
    try{
      const _this = this;
      let arrayOfFiles = [];
      arrayOfFiles = fs.readdirSync(path.join(__dirname, "./xml"))

      if(arrayOfFiles.length){
        arrayOfFiles.forEach(function(name){
          _this.readFiles(name);
        })
      }      
    }catch(e){
      console.warn('Erro ao obter o nome de todos os arquivos. ' + e);
    }
  }

  readFiles(fileName){
    try{
      const xmlData = fs.readFileSync(path.join(__dirname, './xml/' + fileName), 'utf8');      

      const options = {
        ignoreAttributes : false
      };
  
      this.researchersList.push(parser.parse(xmlData, options));      
    }catch(e){
      console.warn('Erro ao ler arquivo ' + fileName + '. ' + e);
    }
  }

  getFileInfo(){
    try{ 
      const _this = this;
      this.nodeList = [];
      
      if(this.researchersList.length){
        this.researchersList.forEach(function(researcher) {
          let authorDetails = new AuthorDetails(researcher['CURRICULO-VITAE']);
          let bibliographicProduction = researcher['CURRICULO-VITAE']['PRODUCAO-BIBLIOGRAFICA']; 
          
          authorDetails = authorDetails.getAuthorDetails();
          new BibliographicProduction(bibliographicProduction, authorDetails, _this.nodeList);
        });
      } 
    }catch(e){
      console.warn('Erro ao obter informação dos arquivos. ' + e);
    }
  }

  getList(){
    return this.nodeList;
  }

  getInitialList() {
    return this.researchersList;
  }
}

module.exports = NodeList;