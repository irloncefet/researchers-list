const express = require("express");
const path = require("path");
const app = express();
const authorRoute = require('./routes/AuthorRoute');
const bProductionRoute = require('./routes/BProductionRoute');
const authorProductionEdgeRoute = require('./routes/AuthorProductionEdgeRoute');
const interestAreaRoute = require('./routes/InterestAreaRoute');
const authorAreaEdgeRoute = require('./routes/AuthorAreaEdgeRoute');
const graphsRoute = require('./routes/GraphsRoute');
const generalRoute = require('./routes/GeneralRoute');
const errorRoute = require('./routes/ErrorRoute');

app.set("view engine", "ejs");
app.set("views", "./server/views/pages");
app.use("/assets", express.static(path.join(__dirname, "../dist/assets")));

app.use(bProductionRoute);
app.use(authorRoute);
app.use(authorProductionEdgeRoute);
app.use(interestAreaRoute);
app.use(authorAreaEdgeRoute);
app.use(graphsRoute);
app.use(generalRoute);
app.use(errorRoute);

app.listen(8080);
console.log("Listening to port 8080...");
