const AuthorAreaEdgeModel =  require("../models/AuthorAreaEdgeModel");
class AuthorAreaEdgeController{
    constructor(){
        this.authorAreaEdgeModel = new AuthorAreaEdgeModel();
    }

    async getAll(req, res){
        res.setHeader("Content-Type", "application/json");
        let responseData = []; 
        let searchTerms = [];
                
        const paramAuthor = req.query.author;
        const paramArea = req.query.area;

        if(paramAuthor){
            searchTerms.push({'fieldName' : 'author', "fieldValue": paramAuthor});
        }
        if(paramArea){
            searchTerms.push({'fieldName' : 'area', "fieldValue": paramArea});
        }
        
        if(searchTerms.length){
            responseData = await this.authorAreaEdgeModel.getByFields(searchTerms);
        }else{
            responseData = await this.authorAreaEdgeModel.getAll();
        }        
        res.status(200).end(JSON.stringify(responseData));
    }
}

module.exports = AuthorAreaEdgeController;