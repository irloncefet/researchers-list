const AuthorModel =  require("../models/AuthorModel");
class AuthorController{
    constructor(){
        this.authorModel = new AuthorModel();
    }

    async getAll(req, res){
        res.setHeader("Content-Type", "application/json");
        let responseData = []; 
        let searchTerms = [];
                
        const paramIdCNPQ = req.query.idcnpq;
        const paramName = req.query.name;
        const paramResume = req.query.resume;

        if(paramIdCNPQ){
            searchTerms.push({'fieldName' : 'idCNPQ', "fieldValue": paramIdCNPQ});
        }
        if(paramName){
            searchTerms.push({'fieldName' : 'name', "fieldValue": paramName});
        }
        if(paramResume){
            searchTerms.push({'fieldName' : 'resume', "fieldValue": paramResume});
        }
        
        if(searchTerms.length){
            responseData = await this.authorModel.getByFields(searchTerms);
        }else{
            responseData = await this.authorModel.getAll();
        }        
        res.status(200).end(JSON.stringify(responseData));
    }
}

module.exports = AuthorController;