const AuthorProductionEdgeModel =  require("../models/AuthorProductionEdgeModel");
class AuthorProductionEdgeController{
    constructor(){
        this.authorProductionEdgeModel = new AuthorProductionEdgeModel();
    }

    async getAll(req, res){
        res.setHeader("Content-Type", "application/json");
        let responseData = []; 
        let searchTerms = [];
                
        const paramAuthor = req.query.author;
        const paramBProduction = req.query.bproduction;

        if(paramAuthor){
            searchTerms.push({'fieldName' : 'author', "fieldValue": paramAuthor});
        }
        if(paramBProduction){
            searchTerms.push({'fieldName' : 'bproduction', "fieldValue": paramBProduction});
        }
        
        if(searchTerms.length){
            responseData = await this.authorProductionEdgeModel.getByFields(searchTerms);
        }else{
            responseData = await this.authorProductionEdgeModel.getAll();
        }        
        res.status(200).end(JSON.stringify(responseData));
    }
}

module.exports = AuthorProductionEdgeController;