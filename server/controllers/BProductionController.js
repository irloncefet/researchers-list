const BProductionModel =  require("../models/BProductionModel");
class BProductionController{
    constructor(){
        this.bProductionModel = new BProductionModel();
    }

    async getAll(req, res){
        res.setHeader("Content-Type", "application/json");        
        let responseData = []; 
        let searchTerms = [];
                
        const paramDoi = req.query.doi;
        const paramTitle = req.query.title;
        const paramType = req.query.type;
        const paramYear = req.query.year;

        if(paramDoi){
            searchTerms.push({'fieldName' : 'doi', "fieldValue": paramDoi});
        }
        if(paramTitle){
            searchTerms.push({'fieldName' : 'title', "fieldValue": paramTitle});
        }
        if(paramType){
            searchTerms.push({'fieldName' : 'type', "fieldValue": paramType});
        }
        if(paramYear){
            searchTerms.push({'fieldName' : 'year', "fieldValue": paramYear});
        }
        
        if(searchTerms.length){
            responseData = await this.bProductionModel.getByFields(searchTerms);
        }else{
            responseData = await this.bProductionModel.getAll();
        }
        res.status(200).end(JSON.stringify(responseData));
    }
}

module.exports = BProductionController;