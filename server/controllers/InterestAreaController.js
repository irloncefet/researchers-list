const InterestAreaModel =  require("../models/InterestAreaModel");
class InterestAreaController{
    constructor(){
        this.areaModel = new InterestAreaModel();
    }

    async getAll(req, res){
        res.setHeader("Content-Type", "application/json");
        let responseData = []; 
        let searchTerms = [];
                
        const paramName = req.query.name;

        if(paramName){
            searchTerms.push({'fieldName' : 'name', "fieldValue": paramName});
        }
        
        if(searchTerms.length){
            responseData = await this.areaModel.getByFields(searchTerms);
        }else{
            responseData = await this.areaModel.getAll();
        }        
        res.status(200).end(JSON.stringify(responseData));
    }
}

module.exports = InterestAreaController;