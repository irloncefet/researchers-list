const DataBase = require("./DataBase");

class AuthorAreaEdgeModel {
  constructor() {
    this.dbConnection = new DataBase();
  }

  async getAll() {
    try {
      const session = await this.dbConnection.startSession();
      let queryResult = await session
        .query("select out, in from Possui")
        .all();

      this.dbConnection.closeSession();

      return queryResult;
    } catch (e) {
      console.log("Erro ao listar todas conexões entre autores e áreas de interesse. " + e);
    }
  }

  async getByFields(fields) {
    try {
      let query = "select in, out from Possui where ";

      fields.forEach((element, index) => {
        let queryConector = "";

        if (fields.length > 1 && index < fields.length - 1) {
          queryConector = "and ";
        }

        switch (element.fieldName) {
          case "name":
            query =
              query +
              " " +
              element.fieldName +
              " like '%" +
              element.fieldValue +
              "%' " +
              queryConector;
            break;
          default:
            query =
              query +
              " " +
              element.fieldName +
              " = '" +
              element.fieldValue +
              "' " +
              queryConector;
        }
      });

      const session = await this.dbConnection.startSession();
      let queryResult = await session.query(query).all();
      this.dbConnection.closeSession();

      return queryResult;
    } catch (e) {
      console.log("Erro ao listar conexões entre autores e áreas de interesse. " + e);
    }
  }
}

module.exports = AuthorAreaEdgeModel;
