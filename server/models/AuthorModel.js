const DataBase = require("./DataBase");

class AuthorModel {
  constructor() {
    this.dbConnection = new DataBase();
  }

  async getAll() {
    try {
      const session = await this.dbConnection.startSession();
      let queryResult = await session
        .query("select @rid, name, resume, idCNPQ from Autor")
        .all();

      this.dbConnection.closeSession();

      return queryResult;
    } catch (e) {
      console.log("Erro ao listar todos os autores. " + e);
    }
  }

  async getByFields(fields) {
    try {
      let query = "select @rid, name, resume, idCNPQ from Autor where ";

      fields.forEach((element, index) => {
        let queryConector = "";

        if (fields.length > 1 && index < fields.length - 1) {
          queryConector = "and ";
        }

        switch (element.fieldName) {
          case "name":
          case "resume":
            query =
              query +
              " " +
              element.fieldName +
              " like '%" +
              element.fieldValue +
              "%' " +
              queryConector;
            break;
          default:
            query =
              query +
              " " +
              element.fieldName +
              " = '" +
              element.fieldValue +
              "' " +
              queryConector;
        }
      });

      const session = await this.dbConnection.startSession();
      let queryResult = await session.query(query).all();
      this.dbConnection.closeSession();

      return queryResult;
    } catch (e) {
      console.log("Erro ao listar autores. " + e);
    }
  }
}

module.exports = AuthorModel;
