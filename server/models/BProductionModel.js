const DataBase = require("./DataBase");

class BProductionModel {
  constructor() {
    this.dbConnection = new DataBase();
  }

  async getAll() {
    try {
      const session = await this.dbConnection.startSession();
      let queryResult = await session
        .query("select @rid, title, doi, type, year from ProducaoBibliografica")
        .all();

      this.dbConnection.closeSession();

      return queryResult;
    } catch (e) {
      console.log("Erro ao listar todas as produções bibliográficas. " + e);
    }
  }

  async getByFields(fields) {
    try {
      let query =
        "select @rid, title, doi, type, year from ProducaoBibliografica where ";

      fields.forEach((element, index) => {
        let queryConector = "";

        if (fields.length > 1 && index < fields.length - 1) {
          queryConector = "and ";
        }

        switch (element.fieldName) {
          case "title":
            query =
              query +
              " " +
              element.fieldName +
              " like '%" +
              element.fieldValue +
              "%' " +
              queryConector;
            break;
          case "year":
            let yearRange = element.fieldValue.split("-");

            if (yearRange.length > 1) {
              query =
                query +
                " " +
                element.fieldName +
                " between '" +
                yearRange[0] +
                "' and '" +
                yearRange[1] +
                "' " +
                queryConector;
            } else {
              query =
                query +
                " " +
                element.fieldName +
                " = '" +
                element.fieldValue +
                "' " +
                queryConector;
            }
            break;
          default:
            query =
              query +
              " " +
              element.fieldName +
              " = '" +
              element.fieldValue +
              "' " +
              queryConector;
        }
      });

      const session = await this.dbConnection.startSession();
      let queryResult = await session.query(query).all();
      this.dbConnection.closeSession();

      return queryResult;
    } catch (e) {
      console.log("Erro ao listar as produções bibliográficas. " + e);
    }
  }
}

module.exports = BProductionModel;
