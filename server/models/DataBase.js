const OrientDBClient = require("orientjs").OrientDBClient;

class DataBase{
    async startSession(){
        this.client = await OrientDBClient.connect({
            host: "localhost",
            port: 2424
        });
        
        this.session = await this.client.session({ name: "aegirdbv2", username: "root", password: "agoravai2022" });

        return this.session;
    }

    closeSession(){
        this.session.close();
        this.client.close();
    }
}

module.exports = DataBase;