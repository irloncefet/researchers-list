const express = require("express");
const router = express.Router();
const AuthorAreaEdgeController = require('../controllers/AuthorAreaEdgeController');

const controller = new AuthorAreaEdgeController();

router.get("/areaconnection/all", function (req, res) {
  console.log("request was made: " + req.url);
  controller.getAll(req, res);  
});

module.exports = router;