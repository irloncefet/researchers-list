const express = require("express");
const router = express.Router();
const AuthorProductionEdgeController = require('../controllers/AuthorProductionEdgeController');

const controller = new AuthorProductionEdgeController();

router.get("/devconnection/all", function (req, res) {
  console.log("request was made: " + req.url);
  controller.getAll(req, res);  
});

module.exports = router;