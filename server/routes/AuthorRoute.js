const express = require("express");
const router = express.Router();
const AuthorController = require('../controllers/AuthorController');

const controller = new AuthorController();

router.get("/author", function (req, res) {
  console.log("request was made: " + req.url);
  res.setHeader("Content-Type", "text/html");
  res.status(200).render("author-details");
});

router.get("/author/all", function (req, res) {
  console.log("request was made: " + req.url);
  controller.getAll(req, res);  
});

module.exports = router;