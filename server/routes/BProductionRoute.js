const express = require("express");
const router = express.Router();
const NodeList = require("../NodeList");
const nodeList = new NodeList();

const BProductionController = require('../controllers/BProductionController');

const controller = new BProductionController();

router.get("/node-connections", function (req, res) {
  console.log("request was made: " + req.url);
  res.setHeader("Content-Type", "application/json");
  res.status(200).end(JSON.stringify(nodeList.getList()));
});

router.get("/node-initial", function (req, res) {
  console.log("request was made: " + req.url);
  res.setHeader("Content-Type", "application/json");
  res.status(200).end(JSON.stringify(nodeList.getInitialList()));
});

router.get("/production/all", function (req, res) {
  console.log("request was made: " + req.url);
  controller.getAll(req, res);  
});


module.exports = router;