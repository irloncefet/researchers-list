const express = require("express");
const router = express.Router();

router.get("*", function (req, res) {
  console.log("request was made: " + req.url);
  res.setHeader("Content-Type", "text/html");
  res.status(404).render("error-404");
});

module.exports = router;