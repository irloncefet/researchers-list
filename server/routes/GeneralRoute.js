const express = require("express");
const router = express.Router();

router.get("/login", function (req, res) {
  console.log("request was made: " + req.url);
  res.setHeader("Content-Type", "text/html");
  res.status(200).render("login");
});

router.get("/search", function (req, res) {
  console.log("request was made: " + req.url);
  res.setHeader("Content-Type", "text/html");
  res.status(200).render("search");
});

router.get("/", function (req, res) {
  console.log("request was made: " + req.url);
  res.setHeader("Content-Type", "text/html");
  res.status(200).render("index");
});

module.exports = router;