const express = require("express");
const router = express.Router();

router.get("/co-autoria", function (req, res) {
  console.log("request was made: " + req.url);
  res.setHeader("Content-Type", "text/html");
  res.status(200).render("graph-productions");
});

router.get("/areas-de-interesse", function (req, res) {
  console.log("request was made: " + req.url);
  res.setHeader("Content-Type", "text/html");
  res.status(200).render("graph-interest-area");
});

router.get("/distancia", function (req, res) {
    console.log("request was made: " + req.url);
    res.setHeader("Content-Type", "text/html");
    res.status(200).render("graph-productions");
  });

module.exports = router;