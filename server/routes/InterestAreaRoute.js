const express = require("express");
const router = express.Router();
const InterestAreaController = require('../controllers/InterestAreaController');

const controller = new InterestAreaController();

router.get("/area/all", function (req, res) {
  console.log("request was made: " + req.url);
  controller.getAll(req, res);  
});

module.exports = router;