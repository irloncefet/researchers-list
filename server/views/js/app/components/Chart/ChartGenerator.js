import "../../../lib/Chart";

export default class ChartGenerator {
    constructor(){
        this.selectors()
        this.events();
        this.drawnChart();
    }

    selectors(){
        this.chartContainer = $('#chart-productions');
    }

    events(){
        const _this = this;

        $(window).on('updateChart', function(e){
            const graph = e.graph;
            const totalEventsWorks = graph.elements("node[type = 'trabalho-em-eventos']").length;
            const totalArticles = graph.elements("node[type = 'artigo-publicado']").length;
            const totalBookCap = graph.elements("node[type = 'capitulo-de-livro-publicado']").length;
            const totalBooks = graph.elements("node[type = 'livro-publicado-organizado']").length;
            _this.updateChart(totalEventsWorks, totalArticles, totalBookCap, totalBooks);
        })
    }

    drawnChart(){
        const labels = ['Trabalho em eventos', 'Artigo', 'Capítulo de livro', 'Livro'];
        const data = [0,0,0,0];
        let bgColors = [];

        let colorModifier = 0;
        
        for(let i = 0; i < labels.length; i ++){

            bgColors.push('hsl(195, 100%, '+ Math.abs(50 - colorModifier) + '%)');

            colorModifier += 7;
        }

        this.chart = new Chart("chart-productions", {
            type: "bar",
            data: {
                labels: labels,
                datasets: [{
                    backgroundColor: bgColors,
                    data: data
                }]
            },
            options: {
                legend: {display: false},
                plugins: {
                    title: {
                        display: false
                    }
                },
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
          });
    }

    updateChart(totalEventsWorks, totalArticles, totalBookCap, totalBooks){
        const values = [totalEventsWorks, totalArticles, totalBookCap, totalBooks];

        this.chart.data.datasets[0].data = values;
        this.chart.update();
    }
}