import TextToCss from "../../utils/TextToCss";

export default class CoauthorDrawer {
  constructor(graphGenerator) {
    this.graphGenerator = graphGenerator;
    this.getNodos();
  }

  async getNodos() {
    const _this = this;

    const authorsData = await this.getAuthors();
    const bProductionsData = await this.getBProductions();
    const connectionsData = await this.getConnections();

    this.drawnAuthors(authorsData);
    this.drawnBProductions(bProductionsData);
    this.drawnConnections(connectionsData);

    this.graphGenerator
      .layout({
        name: "cose",
        fit: true,
        idealEdgeLength: function (edge) {
          return 100;
        },
        edgeElasticity: function (edge) {
          return 300;
        },
        padding: 30,
        nodeOverlap: 4,
        nodeRepulsion: function (node) {
          return 2048;
        },
        nodeDimensionsIncludeLabels: false,
        componentSpacing: 300,
      })
      .run();

    $(window).trigger({ type: "calculateMetrics", graph: _this.graphGenerator });

    $(window).trigger({ type: "initiateFilter", graph: _this.graphGenerator });
  }

  getAuthors() {
    return $.ajax({
      url: "/author/all",
    });
  }

  getBProductions() {
    return $.ajax({
      url: "/production/all",
    });
  }

  getConnections() {
    return $.ajax({
      url: "/devconnection/all",
    });
  }

  drawnAuthors(data) {
    const _this = this;

    data.map((author) => {
      let authorId = TextToCss(author.name);

      if (!_this.getGraphElementById(authorId).length && authorId) {
        _this.graphGenerator.add({
          group: "nodes",
          data: {
            id: authorId,
            name: author.name,
            type: "author",
            degree: 1,
            details: author.resume,
            idCNPQ: author.idCNPQ,
            dbId: "n" + TextToCss(author["@rid"]),
          },
        });
      }
    });
  }

  drawnBProductions(data) {
    const _this = this;

    data.map((project) => {
      let projectId = TextToCss(project.title);

      if (!_this.getGraphElementById(projectId).length && projectId) {
        _this.graphGenerator.add({
          group: "nodes",
          data: {
            id: projectId,
            name: project.title,
            type: project.type,
            doi: project.doi,
            year: project.year,
            dbId: "n" + TextToCss(project["@rid"]),
          },
        });
      }
    });
  }

  drawnConnections(data) {
    const _this = this;

    data.map((connection) => {
      let source = _this.getGraphElementByDBId(connection.out);
      let target = _this.getGraphElementByDBId(connection.in);

      if (source.length && target.length) {
        if (
          !_this.getGraphElementById(
            "n" + TextToCss(connection.out) + "----n" + TextToCss(connection.in)
          ).length
        ) {
          _this.graphGenerator.add({
            group: "edges",
            data: {
              id:
                "n" +
                TextToCss(connection.out) +
                "----n" +
                TextToCss(connection.in),
              source: source[0].id(),
              target: target[0].id(),
            },
          });

          const nodeDegree = source[0].data("degree");
          source[0].data("degree", nodeDegree + 1);
        }
      }
    });
  }

  getGraphElementById(id) {
    return this.graphGenerator.getElementById(id);
  }

  getGraphElementByDBId(dbid) {
    return this.graphGenerator.nodes('[dbId *= "' + TextToCss(dbid) + '"]');
  }
}
