import noUiSlider from "nouislider";

export default class FilterNodes {
  constructor() {
    this.removedElements = [];
    sessionStorage.removeItem('filters');
    
    this.selects();
    this.events();
  }

  selects() {
    this.slider = document.querySelector('.graph__filter__year-slider');
  }

  events() {
    $(window).on("initiateFilter", (e) => {
      this.graphGenerator = e.graph;
      this.createFilters();
      this.createYearRangeSlider();
    });
  }

  createYearRangeSlider() {
    var finalYear, initialYear;

    finalYear = this.graphGenerator.nodes().max(node => {
      return parseInt(node.data('year'));
    }).value;

    initialYear = this.graphGenerator.nodes().min(node => {
      return parseInt(node.data('year'));
    }).value;

    noUiSlider.create(this.slider, {
      start: [initialYear, finalYear],
      connect: true,
      tooltips: true,
      range: {
          'min': initialYear,
          'max': finalYear
      },
      format: {
        from: function(value) {
          return parseInt(value);
        },
        to: function(value) {
          return parseInt(value);
        }
      }
    });

    this.slider.noUiSlider.on('update', (values, handle) => {
      this.applyFilters('year', values);
    });
  }

  createFilters() {
    var types = [];

    var nodesFound = this.graphGenerator.nodes().filter(node => {
      return node.data('type') != 'author';
    });

    var list = nodesFound.nodes().map(ele => {
      var type = ele.data('type');
      var formatedType = type.replaceAll("-", " ");

      if (types.indexOf(type) < 0) {
        types.push(ele.data('type'));
        formatedType = formatedType[0].toUpperCase() + formatedType.slice(1);

        return `
          <li class="graph__filter__item">
            <input id="${type}" name="${type}" data=type="type" type="checkbox">
            <label for="${type}">${formatedType}</label>
          </li>
        `;
      }
    });

    $(".graph__filter__list").append(list);

    $(".graph__filter__item input").on("change", (e) => {
      const element = $(e.target);
      this.applyFilters('type', element);
    })
  }

  applyFilters(type, value) {
    var selectedFilters = this.sincronizeSessionStorage(type, value);
    var notConnected;

    if(this.removedElements.length)
      this.removedElements.restore();

    notConnected = this.filterYear(selectedFilters);
    notConnected.merge(this.filterNodes(selectedFilters));

    this.removedElements = this.graphGenerator.remove(notConnected);

    $(window).trigger({
      type: "calculateMetrics",
      graph: this.graphGenerator,
    });

    $(window).trigger({
      type: "updateChart",
      graph: this.graphGenerator,
    });
  }

  sincronizeSessionStorage(type, value) {
    var filters = sessionStorage.getItem('filters');
    filters = JSON.parse(filters);

    if (!filters) filters = {};

    if(type == 'year') {
      filters["year"] = value;
    } else {
      var element = $(value);
      var type = element.attr("id");

      if(!$(element)[0].checked) {
        var types = filters.type.filter((ele) => { 
            return ele != type; 
        });

        filters["type"] = types;
      } else {
        if(filters.type === undefined) filters["type"] = [];
        filters.type.push(type);
      }
    }

    sessionStorage.setItem('filters', JSON.stringify(filters));
    return filters;
  }

  filterNodes(selectedFilters) {
    var connected, notConnected;
    var nodesFound = [];
    var types = selectedFilters.type;

    if(types === undefined) return;
    if(types.length == 0) return;

    types.map((type, index) => {

      if (index == 0) {
        nodesFound = this.graphGenerator.nodes().filter(node => {
          return node.data('type') == type;
        });
    
        connected = nodesFound.union(nodesFound.neighborhood());
        notConnected = this.graphGenerator.elements().not(connected);
      } else {
        nodesFound = nodesFound.union(this.graphGenerator.nodes().filter(node => {
          return node.data('type') == type;
        }));

        connected = connected.union(nodesFound.union(nodesFound.neighborhood()));
      }
    });

    notConnected = this.graphGenerator.elements().not(connected);
    return notConnected;
  }

  filterYear(values) {
    values = values.year;
    const initialYear = values[0];
    const finalYear = values[1];
    var connected, notConnected;
    let nodesFound = [];

    nodesFound = this.graphGenerator.nodes().filter(node => {
      let year = node.data('year');
      return year >= initialYear && year <= finalYear;
    });

    connected = nodesFound.union(nodesFound.neighborhood());
    notConnected = this.graphGenerator.elements().not(connected);

    return notConnected;
  }
}