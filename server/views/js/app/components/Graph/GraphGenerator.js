import cytoscape from "cytoscape";
import GraphStyles from "./GraphStyles";
import CoauthorDrawer from "./CoauthorDrawer";
import InterestAreaDrawer from "./InterestAreaDrawer";
export default class GraphGenerator {
  constructor() {
    this.selectors();

    this.graphGenerator = cytoscape({
      container: this.graphContainer,
      style: GraphStyles,
    });

    this.drawnGraph();
    this.events();
  }

  selectors() {
    this.graphContainer = $(".graph__content");
    this.buttonCenter = $(".graph__buttons__center");
    this.buttonZoomIn = $(".graph__buttons__zoom-in");
    this.buttonZoomOut = $(".graph__buttons__zoom-out");
  }

  events() {
    const _this = this;

    this.graphGenerator.on("select", "node", function (evt) {
      const node = evt.target;

      _this.graphGenerator
        .elements()
        .difference(node.outgoers().union(node.incomers()))
        .not(node)
        .addClass("semitransp");

      node
        .addClass("highlight")
        .outgoers()
        .union(node.incomers())
        .addClass("highlight");

      $(window).trigger({ type: "showNodeInfo", node: node });
    });

    this.graphGenerator.on("unselect", "node", function (evt) {
      const node = evt.target;
      _this.graphGenerator.elements().removeClass("semitransp");
      node
        .removeClass("highlight")
        .outgoers()
        .union(node.incomers())
        .removeClass("highlight");

      $(window).trigger({ type: "hideNodeInfo" });
    });

    this.buttonCenter.on("click", function () {
      _this.graphGenerator.fit();
    });

    this.buttonZoomIn.on("click", function () {
      let actualZoomLevel = _this.graphGenerator.zoom() + 0.5;
      _this.graphGenerator.zoom(actualZoomLevel);
    });

    this.buttonZoomOut.on("click", function () {
      let actualZoomLevel = _this.graphGenerator.zoom() - 0.5;

      if (actualZoomLevel < 0) {
        actualZoomLevel = 0.05;
      }
      _this.graphGenerator.zoom(actualZoomLevel);
    });
  }

  drawnGraph() {
    if ($("body").hasClass("search-result-graph__production")) {
      this.coauthorGraph = new CoauthorDrawer(this.graphGenerator);
    }

    if ($("body").hasClass("search-result-graph__interest-area")) {
      this.interestAreaGraph = new InterestAreaDrawer(this.graphGenerator);
    }
  }

  getGraphGenerator() {
    return this.graphGenerator;
  }
}
