const GraphStyles = [
    {
      selector: "node",
      style: {
        // 'contentFontWeight': 'bold',
      },
    },
    {
      selector: "[type = 'trabalho-em-eventos']",
      style: {
        "background-color": "#a6a6a6",
        shape: "round-triangle",
      },
    },
    {
      selector: "[type = 'artigo-publicado']",
      style: {
        "background-color": "#a6a6a6",
        shape: "round-triangle",
      },
    },
    {
      selector: "[type = 'livro-publicado-organizado']",
      style: {
        "background-color": "#a6a6a6",
        shape: "round-triangle",
      },
    },
    {
      selector: "[type = 'capitulo-de-livro-publicado']",
      style: {
        "background-color": "#a6a6a6",
        shape: "round-triangle",
      },
    },
    {
      selector: "[type = 'author']",
      style: {
        "background-color": "mapData(degree, 0, 100, #66e0ff, #00264d)",
        width: "mapData(degree, 0, 100, 25, 150)",
        height: "mapData(degree, 0, 100, 25, 150)",
      },
    },
    {
      selector: "edge",
      style: {
        width: 3,
        "line-color": "#ccc",
        "target-arrow-color": "#ccc",
        "curve-style": "bezier",
      },
    },
    {
      selector: ":selected",
      style: {
        "background-color": "#ffc800",
        "line-color": "#ffc800",
        content: "data(name)",
      },
    },
    {
      selector: "node.semitransp",
      style: { opacity: "0.3", "z-index": "1" },
    },
    {
      selector: "edge.semitransp",
      style: { opacity: "0.2", "z-index": "1" },
    },
    {
      selector: "node.highlight",
      style: { opacity: "1", "z-index": "2" },
    },
    {
      selector: "[type = 'author'].highlight",
      style: {
        'content': 'data(name)'
      },
    },
    {
      selector: "edge.highlight",
      style: { opacity: "1", "z-index": "2", 'content': 'data(name)' },
    },
];

export default GraphStyles;