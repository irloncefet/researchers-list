import TextToCss from "../../utils/TextToCss";

export default class InterestAreaDrawer {
  constructor(graphGenerator) {
    this.graphGenerator = graphGenerator;
    this.getNodos();
  }

  async getNodos() {
    const _this = this;

    const authorsData = await this.getAuthors();
    const areaData = await this.getInterestArea();
    const connectionsData = await this.getConnections();

    this.drawnAuthors(authorsData);
    this.drawnInterestArea(areaData);
    this.drawnConnections(connectionsData);

    this.graphGenerator
      .layout({
        name: "cose"
      })
      .run();

    $(window).trigger({ type: "calculateMetrics", graph: _this.graphGenerator });

    $(window).trigger({ type: "updateChart", graph: _this.graphGenerator });

    // $(window).trigger({ type: "initiateFilter", graph: _this.graphGenerator });
  }

  getAuthors() {
    return $.ajax({
      url: "/author/all",
    });
  }

  getInterestArea() {
    return $.ajax({
      url: "/area/all",
    });
  }

  getConnections() {
    return $.ajax({
      url: "/areaconnection/all",
    });
  }

  drawnAuthors(data) {
    const _this = this;

    data.map((author) => {
      let authorId = TextToCss(author.name);

      if (!_this.getGraphElementById(authorId).length && authorId) {
        _this.graphGenerator.add({
          group: "nodes",
          data: {
            id: authorId,
            name: author.name,
            type: "author",
            details: author.resume,
            degree: 1,
            idCNPQ: author.idCNPQ,
            dbId: "n" + TextToCss(author["@rid"]),
          },
        });
      }
    });
  }

  drawnInterestArea(data) {
    const _this = this;

    data.map((area) => {
      let areaId = TextToCss(area.name);

      if (!_this.getGraphElementById(areaId).length && areaId) {
        _this.graphGenerator.add({
          group: "nodes",
          data: {
            id: areaId,
            name: area.name,
            type: "interestArea",
            degree: 1,
            dbId: "n" + TextToCss(area["@rid"]),
          },
        });
      }
    });
  }

  drawnConnections(data) {
    const _this = this;

    data.map((connection) => {
      let source = _this.getGraphElementByDBId(connection.out);
      let target = _this.getGraphElementByDBId(connection.in);

      if (source.length && target.length) {
        if (
          !_this.getGraphElementById(
            "n" + TextToCss(connection.out) + "----n" + TextToCss(connection.in)
          ).length
        ) {
          _this.graphGenerator.add({
            group: "edges",
            data: {
              id:
                "n" +
                TextToCss(connection.out) +
                "----n" +
                TextToCss(connection.in),
              source: source[0].id(),
              target: target[0].id(),
            },
          });
          const nodeDegree = target[0].data("degree");
          target[0].data("degree", nodeDegree + 1);
        }
      }
    });
  }

  getGraphElementById(id) {
    return this.graphGenerator.getElementById(id);
  }

  getGraphElementByDBId(dbid) {
    return this.graphGenerator.nodes('[dbId *= "' + TextToCss(dbid) + '"]');
  }
}
