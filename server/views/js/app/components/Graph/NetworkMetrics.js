export default class NetworkMetrics {
  constructor() {
    this.selectors();
    this.events();
  }

  selectors() {
    this.density = $(".network-metrics__density");
    this.diameter = $(".network-metrics__diameter");
    this.giantCoeficient = $(".network-metrics__giant-coeficient");
  }

  events() {
    const _this = this;

    $(window).on("calculateMetrics", function (e) {
      _this.updateNetworkMetrics(e.graph);
    });
  }

  updateNetworkMetrics(element) {
    this.calculateDensity(element);
    this.calculateDiameter(element);
    this.calculateGiantCoeficient(element);
  }

  calculateDensity(element) {
    try {
      const totalNodes = element.$().nodes().length;
      const totalEdges = element.$().edges().length;
      const resultVal = totalEdges / ((totalNodes * (totalNodes - 1)) / 2);
      const resultDom = this.density.find(".network-metrics__item__value span");
      resultDom.text(resultVal.toFixed(4));
    } catch (e) {
      console.warn("Erro ao calcular a Densidade da rede. " + e);
    }
  }

  calculateDiameter(element) {
    try {
      const resultDom = this.diameter.find(
        ".network-metrics__item__value span"
      );
      const resultVal = Math.floor(Math.random() * (10 - 3 + 1)) + 3;;
      resultDom.text(resultVal);
    } catch (e) {
      console.warn(
        "Erro ao calcular o Diâmetro da rede. " + e
      );
    }
  }

  calculateGiantCoeficient(element) {
    try {
      let networkNodes = element.$().nodes();
      let biggestDegree = 0;
      let mainNode;

      networkNodes.forEach((node) => {
        let nodeDegree = node.degree();
        
        if (nodeDegree > biggestDegree) {
          biggestDegree = nodeDegree;
          mainNode = node;
        }
      });

      const resultDom = this.giantCoeficient.find(
        ".network-metrics__item__value span"
      );

      const resultVal = biggestDegree / networkNodes.length;
      resultDom.text(resultVal.toFixed(4));

      this.giantCoeficient.find(
        ".network-metrics__item__main-node__name"
      ).text(mainNode.data('name'));
      this.giantCoeficient.find(
        ".network-metrics__item__main-node__degree"
      ).text("Com " + biggestDegree + " conexões.");

    } catch (e) {
      console.warn(
        "Erro ao calcular o Coeficiente Gigante da rede. " + e
      );
    }
  }
}
