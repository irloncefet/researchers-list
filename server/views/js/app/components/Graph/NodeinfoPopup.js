export default class NodeinfoPopup {
  constructor(graphGenerator) {
    this.graphGenerator = graphGenerator;
    this.selectors();
    this.events();
  }

  selectors() {
    this.nodeinfoPopup = $(".graph__node-info");
    this.nodeName = $(".graph__node-info__name");
    this.nodeBtn = $(".graph__node-info__link");
    this.nodeMetricsList = $(".graph__node-info__metrics ul");
    this.nodeAdjacentList = $(".graph__node-info__adjacent-nodes");
  }

  events() {
    const _this = this;

    $(window).on("showNodeInfo", function (e) {
      _this.showNodeinfoPopup(e.node);
    });

    $(window).on("hideNodeInfo", function () {
      _this.hideNodeinfoPopup();
    });
  }

  updateNodeinfoPopup(element) {
    this.nodeName.text(element.data("name"));
    this.nodeBtn.hide();

    switch (element.data("type")) {
      case "author":
        const details = element.data("details");
        const idCNPQ = element.data("idCNPQ");

        if (details !== undefined && idCNPQ) {
          this.nodeBtn
            .html(
              `
          <a href="/author?${idCNPQ}">
            Ver detalhes
          </a>
        `
            )
            .show();
        } else if (idCNPQ.length) {
          this.nodeBtn
            .html(
              `
          <a href="http://lattes.cnpq.br/./${idCNPQ}" target="_blank">
            Ver curriculo
          </a>
        `
            )
            .show();
        }
        break;
      case "trabalho-em-eventos":
      case "artigo-publicado":
      case "livro-publicado-organizado":
      case "capitulo-de-livro-publicado":
        const doi = element.data("doi");

        if (doi.length)
          this.nodeBtn
            .html(
              `
          <a href="https://doi.org/${doi}" target="_blank">
            Ver detalhes
          </a>
        `
            )
            .show();
        break;
      default:
    }

    const adjacentNodes = element.neighborhood("node");

    if (adjacentNodes.length) {
      this.nodeAdjacentList.show();
      const list = this.nodeAdjacentList.find("ul");
      list.empty();

      this.nodeAdjacentList.find("h3").text("Autores");

      if (element.data("type") == "author" && $('body').hasClass('search-result-graph__production'))
        this.nodeAdjacentList.find("h3").text("Produções bibliográficas");

      if (element.data("type") == "author" && $('body').hasClass('search-result-graph__interest-area'))
        this.nodeAdjacentList.find("h3").text("Áreas de Interesse");

      adjacentNodes.forEach(function (element) {
        list.append(
          `<li data-id="${element.id()}">${element.data("name")}</li>`
        );
      });
    } else {
      this.nodeAdjacentList.hide();
    }

    this.calculateMetrics(element);
  }

  calculateMetrics(element) {
    let centrality = element.degree();
    let proximity = this.graphGenerator.$().cc({ root: "#" + element.id() });
    let betweenness = this.graphGenerator
      .$()
      .bc()
      .betweenness("#" + element.id());
    let clusteringCoefficient = 0;

    let centralityText = `<span>${centrality}</span> conexões diretas com outros nós.`;
    let proximityText = `<span>${proximity.toFixed(2)}</span>`;
    let betweennessText = `<span>${betweenness.toFixed(
      0
    )}</span> caminhos são compostos por este nó.`;
    let clusteringCoefficientText = `<span>${clusteringCoefficient}</span>`;

    this.nodeMetricsList.empty();
    this.drawnMetricsItems("Centralidade", centralityText);
    this.drawnMetricsItems("Intermediação", betweennessText);
    this.drawnMetricsItems("Proximidade", proximityText);
    // this.drawnMetricsItems('Coeficiente de Agrupamento', clusteringCoefficientText);
  }

  drawnMetricsItems(title, value) {
    const item = `<li class="graph__node-info__metric-item">
                    <div class="graph__node-info__title">${title}</div>
                    <div class="graph__node-info__value">
                      ${value}
                    </div>
                  </li>
    `;

    this.nodeMetricsList.append(item);
  }

  showNodeinfoPopup(element) {
    if (element) {
      this.updateNodeinfoPopup(element);
      this.nodeinfoPopup.addClass("active");
    }
  }

  hideNodeinfoPopup() {
    this.nodeinfoPopup.removeClass("active");
  }
}
