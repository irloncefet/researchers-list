import TextToCss from "../../utils/TextToCss";
export default class SearchNodes {
  constructor(graphGenerator) {
    this.graphGenerator = graphGenerator;

    this.selectors();
    this.events();
  }

  selectors() {
    this.searchForm = $(".graph__search__form");
    this.searchInput = $(".graph__search__field");
    this.searchNotFound = $(".graph__search__not-found");
    this.searchResultsFound = $(".graph__search__result");
    this.buttonPrev = $(".graph__search__result__prev");
    this.buttonNext = $(".graph__search__result__next");
    this.buttonClear = $(".graph__search__result__clear-search");
    this.buttonToogleList = $(".graph__search__result__toggle-list");
    this.foundItemsList = $(".graph__search__result__list");
  }

  events() {
    const _this = this;

    this.searchForm.on("submit", function (e) {
      e.preventDefault();

      const searchTerm = _this.searchInput.val();

      if (searchTerm) {
        _this.searchNodes(searchTerm);
      }
    });

    this.buttonPrev.on("click", function () {
      if (_this.currentIndex == 0) {
        _this.currentIndex = _this.totalItemsFound - 1;
      } else {
        _this.currentIndex--;
      }

      _this.focusOnNode(_this.searchResultItems[_this.currentIndex]);

      _this.updateMessageActualIndex(_this.currentIndex + 1);
    });

    this.buttonNext.on("click", function () {
      if (_this.currentIndex == _this.totalItemsFound - 1) {
        _this.currentIndex = 0;
      } else {
        _this.currentIndex++;
      }

      _this.focusOnNode(_this.searchResultItems[_this.currentIndex]);

      _this.updateMessageActualIndex(_this.currentIndex + 1);
    });

    this.buttonClear.on("click", function () {
      _this.searchResultsFound.removeClass("active");
      _this.searchInput.val("");
    });

    this.buttonToogleList.on("click", function () {
      $(this).toggleClass("active");
      _this.foundItemsList.toggleClass("active");
    });
  }

  searchNodes(searchTerm) {
    const formatedTerm = TextToCss(searchTerm);
    let nodesFound = [];

    nodesFound = this.graphGenerator.nodes('[id *= "' + formatedTerm + '"]');

    if (nodesFound.length) {
      this.searchResultItems = nodesFound;
      this.currentIndex = 0;
      this.totalItemsFound = nodesFound.length;

      this.showResultsFound();
    } else {
      this.showNotFound();
    }
  }

  showNotFound() {
    const _this = this;

    this.searchNotFound.fadeIn(200);

    setTimeout(function () {
      _this.searchNotFound.fadeOut(200);
    }, 3000);
  }

  showResultsFound() {
    const _this = this;

    const actualItem = _this.searchResultsFound.find(
      ".graph__search__result__actual"
    );
    const totalFound = _this.searchResultsFound.find(
      ".graph__search__result__total-found"
    );

    const list = _this.foundItemsList;

    _this.searchResultsFound.addClass("active");

    actualItem.text("1");
    totalFound.text(_this.totalItemsFound);

    _this.focusOnNode(_this.searchResultItems[_this.currentIndex]);

    list.empty();

    _this.searchResultItems.forEach(function (element, index) {
      list.append(
        `<li data-id="${element.id()}" data-index="${index}">
            <span>${element.data("name")}</span>
            <button aria-label="Mostrar na rede">
                <i class="btn-center_icon"></i>
            </button>
        </li>`
      );
    });

    list.find('li button').on('click', function(){
        const index = $(this).parent().data('index');

        _this.currentIndex = index;
        _this.focusOnNode(_this.searchResultItems[_this.currentIndex])
        _this.updateMessageActualIndex(_this.currentIndex + 1);
    });
  }

  focusOnNode(node) {
    const _this = this;

    _this.graphGenerator.batch(function () {
      _this.graphGenerator.$("node, edge").unselect();
    });

    node.emit("select");
    node.json({ selected: true });

    _this.graphGenerator.zoom(1);
    _this.graphGenerator.center(node);
  }

  updateMessageActualIndex(newIndex){
    const actualItem = this.searchResultsFound.find(
        ".graph__search__result__actual"
      );

      actualItem.text(newIndex);
  }
}
