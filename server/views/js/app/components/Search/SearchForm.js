export default class SearchForm {
    constructor(searchForm){
        this.searchForm = searchForm;

        this.selectors();
        this.events();
    }
    
    selectors(){
        this.searchInput = this.searchForm.find('input');
        this.searchButton = this.searchForm.find('button');
    }

    events(){
        const _this = this;

        this.searchForm.on('submit', (e)=>{
            e.preventDefault();

            const searchTerm = _this.searchInput.val();

            window.location.href = "/search/?term=" + searchTerm;
        })
    }
}