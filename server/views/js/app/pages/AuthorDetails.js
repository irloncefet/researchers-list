export default class AuthorDetails {
  constructor() {
    this.selectors();
    //this.events();

    this.getElements();
  }

  selectors() {
    this.authorName = $(".author__name");
    this.authorResume = $(".author__resume");
    this.authorGraduation = $(".author__graduation");
  }

  events() {}

  getElements() {
    this.authorId = (window.location.search).replace("?", "");
    var _this = this;

    $.ajax({
      url: "/node-connections",
    }).done((data) => {
      data.map(project => {
        project.authors.map(author => {
          if(author.idCNPQ == _this.authorId) _this.authorDetails = author.details; 
        });
      });  
      
      if (_this.authorDetails) _this.renderPage()
    });
  }

  renderPage() {
    const author = this.authorDetails;
    console.log(author)

    this.authorName.html(`<h1>${author.name}</h1>`);
    this.authorResume.append(`<h2>Resumo</h2>`);
    this.authorResume.append(`<span>${author.resume}</span>`);

    this.authorGraduation.append(`<h2>Formação acadêmica/titulação</h2>`);

    author.academicGraduation.map(element => {
      let html = `
        <article>
          <div>${element.type}</div>
          <div>Instituição: ${element.nameInstitution}</div>
          <div>Curso: ${element.courseName}</div>
        </article>
      `

      this.authorGraduation.append(html);
    }); 

    this.authorGraduation.append(
      `<a href="http://lattes.cnpq.br/./${author.idCNPQ}" target="_blank">Ver mais detalhes na Prataforma Lattes</a>`
    );
  }

  _toggleFilter(element) {
    $(element).parent().toggleClass("active");
  }
}
