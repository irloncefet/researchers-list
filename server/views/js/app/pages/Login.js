export default class Login {
  constructor() {
    this.checkIfIsLoggedin();

    if($('body').hasClass('login')) {
        this.selectors();
        this.events();
    }
  }

  selectors() {
      this.form = $('.login__form');
      this.username = $('.login__form #username');
      this.password = $('.login__form #password');
      this.btnLogin = $('.login__form .btn-login');
  }

  events() {
      const _this = this;

      this.form.on('submit', (e) =>{
        e.preventDefault();

        window.location.href="/";
        _this.setCookie();
      })
  }

  checkIfIsLoggedin() {
    let cookie = this.getCookie("logged-in");

    if (cookie != 1 && !$('body').hasClass('login')) {
      window.location.href="/login";
    }else{
        $('body').addClass('logged-in');
    }
  }

  getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(";");
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == " ") c = c.substring(1);
      if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
  }

  setCookie() {
    let d = new Date();
    d.setTime(d.getTime() + 7 * 24 * 60 * 60 * 1000);
    let expires = "expires=" + d.toUTCString();
    document.cookie = "logged-in=1; " + expires;
  }
}
