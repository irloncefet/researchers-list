import GetUrlParam from "../utils/GetUrlParam";
import "regenerator-runtime/runtime";

export default class Search {
  constructor() {
    this.selectors();
    this.events();
    this.search();
  }

  selectors() {
    this.filterContainer = $(".search-page__filter");
    this.resultContainer = $(".search-page__results");
    this.resultCount = $('.search-page__count');
  }

  events() {
    console.log("here");
  }

  async search() {
    const _this = this;
    const url = window.location.href;
    const searchParam = GetUrlParam(url, "term");

    if (searchParam) {
      const authorsData = await _this.getAuthors(searchParam);
      const productionsData = await _this.getBProductions(searchParam);

      _this.drawnResult(authorsData, productionsData);
    }
  }

  getAuthors(searchParam) {
    return $.ajax({
      url: "/author/all?name=" + searchParam,
    });
  }

  getBProductions(searchParam) {
    return $.ajax({
      url: "/production/all?title=" + searchParam,
    });
  }

  drawnResult(authorsData, productionsData) {
    const authorsQty = authorsData.length;
    const productionQty = productionsData.length;

    if ( authorsQty || productionQty) {
      this.listAuthors(authorsData);
      this.listBProductions(productionsData);

      this.searchResultCount(authorsQty + productionQty);
    } else {
      this.emptySearch();
    }
  }

  listAuthors(data) {
    const _this = this;
    
    data.map((element) => {
      const html = `
          <div class="search-page__results__item">
              <h3 class="search-page__results__item__title">${
                element.name
              }</h3>
              ${
                element.resume
                  ? "<div class='search-page__results__item__resume'>" +
                    element.resume.slice(0, 200).concat('...') +
                    "</div>"
                  : ""
              }
              ${
                element.idCNPQ
                  ? "<a href='/author/?" +
                    element.idCNPQ +
                    "' target='_blank'>Ver detalhes </a>"
                  : ""
              }
          </div>
        `;

      _this.resultContainer.append(html);
    });
  }

  listBProductions(data) {
    const _this = this;

    data.map((element) => {
      const html = `
            <div class="search-page__results__item">
                <h3 class="search-page__results__item__title">${
                  element.title
                }</h3>
                <div class="search-page__results__item__type">${
                  element.type
                }</div>
                <div class="search-page__results__item__year">${
                  element.year
                }</div>
                ${
                  element.doi
                    ? "<a href='https://doi.org/" +
                      element.doi +
                      "' target='_blank'>Ver detalhes </a>"
                    : ""
                }
            </div>
          `;

      _this.resultContainer.append(html);
    });
  }

  emptySearch() {
    this.resultContainer.empty();
    this.resultContainer.addClass("col-12 col-xl-12");

    const html = `
        <div class="search-page__results__empty-search">
            <h2>Busca vazia.</h2>
            <span>Infelizmente não encontramos nenhum resultado, por favor tente utilizar um termo diferente para a sua busca.</span>
        </div>
    `;

    this.resultContainer.append(html);
  }

  searchResultCount(qty){
    this.resultCount.find('span').text(qty);
    this.resultCount.addClass('active');
  }
}
