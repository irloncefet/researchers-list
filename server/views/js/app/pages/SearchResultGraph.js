import GraphGenerator from "../components/Graph/GraphGenerator";
import NodeinfoPopup from "../components/Graph/NodeinfoPopup";
import NetworkMetrics from "../components/Graph/NetworkMetrics";
import SearchNodes from "../components/Graph/SearchNodes";
import FilterNodes from "../components/Graph/FilterNodes";
import ChartGenerator from "../components/Chart/ChartGenerator";
export default class SearchResultGraph {
  constructor() {
    this.selectors();
    this.events();
    
    this.graphGenerator = new GraphGenerator();
    this.networkMetrics = new NetworkMetrics();
    
    this.graph = this.graphGenerator.getGraphGenerator();
    this.nodeinfoPopup = new NodeinfoPopup(this.graph);
    this.searchNodes = new SearchNodes(this.graph);
    this.FilterNodes = new FilterNodes();

    this.chartGenerator = new ChartGenerator();
  }

  selectors() {
    this.filterToggler = $(".graph__filter__title");
  }

  events() {
    const _this = this;

    this.filterToggler.on("click", function () {
      _this._toggleFilter(this);
    });
  }

  _toggleFilter(element) {
    $(element).parent().toggleClass("active");
  }
}
