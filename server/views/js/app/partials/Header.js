import SearchForm from "../components/Search/SearchForm";

export default class Header {
    constructor(){
        this.selectors();

        if(!$('.header').hasClass('header-simple')){
            new SearchForm(this.headerForm);
        }
    }
    
    selectors(){
        this.headerForm = $('.header__search__form');
    }
}