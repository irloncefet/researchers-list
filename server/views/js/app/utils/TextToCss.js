export default function(text){
    if (typeof text == "undefined") {
        return "";
      }
  
      text = text
        .toLowerCase()
        .replace(/\)|\(/g, "")
        .replace(/\./g, "")
        .replace(/,/g, "")
        .replace(/:/g, "-")
        .replace(/ /g, "-")
        .replace(/\//g, "_")
        .replace(/[áàâã]/g, "a")
        .replace(/[ìíĩî]/g, "i")
        .replace(/[éèê]/g, "e")
        .replace(/[óòôõ]/g, "o")
        .replace(/[úùû]/g, "u")
        .replace(/[ç]/g, "c")
        .replace(/[^A-Za-z0-9_-]/g, "");
      return text;
}