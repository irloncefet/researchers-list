import Header from "./app/partials/Header";
import Login from "./app/pages/Login";
import SearchResultGraph from "./app/pages/SearchResultGraph";
import AuthorDetails from "./app/pages/AuthorDetails";
import Search from "./app/pages/Search";

$(document).on("ready", () => {
  new Header();
  new Login();

  if ($("body").hasClass("search-result-graph__production")) {
    new SearchResultGraph();
  }

  if ($("body").hasClass("search-result-graph__interest-area")) {
    new SearchResultGraph();
  }

  if ($("body").hasClass("author-details")) {
    new AuthorDetails();
  }

  if ($("body").hasClass("search-page")) {
    new Search();
  }
});
