const webpack = require('webpack');
/**
 * Configuração do webpack
 * @tutorial https://github.com/webpack/webpack.js.org/tree/v3.11.0/src/content
 * @tutorial https://medium.com/netscape/webpack-3-react-production-build-tips-d20507dba99a
 */
module.exports = {
	entry:{
		main:["regenerator-runtime/runtime.js",'./server/views/js/main.js']
	},
	output: {
		filename: '[name]-bundle.js',
	},
	module: {
		rules: [{
			test: /\.(js|jsx)$/,
			exclude: /(node_modules)/,
			loader: 'babel-loader',
			query: {
				presets: [
					['latest', {
						modules: false
					}]
				]
			}
		}]
	},
	plugins: [
		new webpack.ProvidePlugin({
			'$':'jquery',
			'jQuery':'jquery',
		})
	]
};
